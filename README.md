**Instalador de FreePBX 13, Certified Asterisk 13, Webmin, reglas de Firewall, Fail2Ban y personalizaciones de ALO Global**
# Prerrequisitos
- Esta instalación se debe realizar sobre una instalación vacía de CentOS Linux 7 minimal completamente actualizado
- El servidor debe contar con la red configurada para iniciarse automáticamente, y debe contar con salida a Internet en el momento de la instalación

# Secuencia de instalación
- Copie el archivo freepbx-installer en el servidor. Puede ser en /tmp
- Otorgue al script permisos de ejecución:
```
chmod +x /tmp/freepbx-installer
```
- Ejecute el instalador como root
```
    sudo /tmp/freepbx-installer
```
# Seguimiento de fallos
- Si se produce algún fallo, abra un ticket a este proyecto, anexando la salida del comando. Puede almacenar su salida redirigiendola a un archivo:
```
sudo /tmp/freepbx-installer >> /tmp/freepbx-installer.log
```
